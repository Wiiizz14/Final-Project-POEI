import {ComponentFixture, TestBed} from '@angular/core/testing';

import {GetNavbarComponent} from './get-navbar.component';

describe('GetNavbarComponent', () => {
  let component: GetNavbarComponent;
  let fixture: ComponentFixture<GetNavbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [GetNavbarComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GetNavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
