import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {AuthenticationService} from "../../services/helper/authentication.service";
import {environment} from "../../environments/environment";
import {Category} from "../../interfaces/category";
import {map} from "rxjs/operators";
import {Observable} from "rxjs";
import {CategoryService} from 'src/services/category.service';
import {StudentService} from "../../services/student.service";

@Component({
  selector: 'app-get-navbar',
  templateUrl: './get-navbar.component.html',
  styleUrls: ['./get-navbar.component.css']
})
export class GetNavbarComponent implements OnInit {

  url: string = environment.apiUrl;
  categories!: Observable<Category[]>;
  category!: Observable<Category>;

  currentUserId!: number;
  username!: string | void;
  isAuth: boolean = false;
  isAdmin: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private routeRedir: Router,
    private authenticationService: AuthenticationService,
    private categoryService: CategoryService,
    private studentService: StudentService) {
  }

  ngOnInit(): void {
    //Get category list and sort it by id value
    this.categories = this.categoryService.getCategories().pipe(map((items) => {
      items.sort((a, b) => {
        return a.id < b.id ? -1 : 1;
      });
      return items;
    }));

    //Get auth
    let auth = this.authenticationService.getCurrentUserBasicAuthentication();
    if(auth){
      this.isAuth = true;
    }


    //Get role
    this.isAdmin = this.authenticationService.getIsAdmin();


    //Get currentUserId
    this.username = this.authenticationService.getCurrentUserName();
    this.studentService.getStudentByUsername(this.username).subscribe((student) => this.currentUserId = student.id);
  }

  onHomepageClick() {
    this.routeRedir.navigate(['home']);
  }

  onCategoriesClick() {
    this.routeRedir.navigate(['categories']);
  }

  onCategoryClick(id: number) {
    this.routeRedir.navigate(['categories/' + id]);
  }

  onCreateCategoryClick() {
    this.routeRedir.navigate(['categories/new']);
  }

  onUserClick() {
    this.routeRedir.navigate(['questions/user']);
  }

  onLogoutClick() {
    this.authenticationService.logout();
  }

  onQuestionsClick() {
    this.routeRedir.navigate(['questions']);
  }

  onCreateQuestionsClick() {
    this.routeRedir.navigate(['questions/new']);
  }
}
