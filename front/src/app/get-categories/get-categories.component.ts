import {Component, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {Category} from "../../interfaces/category";
import {Router} from "@angular/router";
import {AuthenticationService} from "../../services/helper/authentication.service";
import {CategoryService} from "../../services/category.service";
import {map} from "rxjs/operators";

@Component({
  selector: 'app-get-categories',
  templateUrl: './get-categories.component.html',
  styleUrls: ['./get-categories.component.css']
})
export class GetCategoriesComponent implements OnInit {

  categories!: Observable<Category[]>;
  isAdmin: boolean = false;

  constructor(
    private route: Router,
    private authenticationService: AuthenticationService,
    private categoryService: CategoryService) {
  }

  ngOnInit(): void {
    //Get category list and sort it by id value
    this.categories = this.categoryService.getCategories().pipe(map((items) => {
      items.sort((a, b) => {
        return a.id < b.id ? -1 : 1;
      });
      return items;
    }));

    //Get role
    this.isAdmin = this.authenticationService.getIsAdmin();
  }

  onCategoryDetailsClick(id: number) {
    this.route.navigate(['categories/' + id]);
  }

  onCategoryUpdateClick(id: number) {
    this.route.navigate(['categories/' + id + "/update"]);
  }

  onCategoryDeleteClick(id: number) {
    this.categoryService.deleteCategory(id).subscribe()
    window.alert('Category deleted !');
    location.reload();
  }
}
