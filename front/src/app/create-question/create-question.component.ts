import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {QuestionService} from "../../services/question.service";
import {Question} from "../../interfaces/question";

@Component({
  selector: 'app-create-question',
  templateUrl: './create-question.component.html',
  styleUrls: ['./create-question.component.css']
})
export class CreateQuestionComponent implements OnInit {

  constructor(
    private route: Router,
    private questionService: QuestionService) {
  }

  ngOnInit(): void {
  }

  onSubmit(questionToCreate: Question) {
    this.questionService.createQuestion(questionToCreate).subscribe();
  }
}
