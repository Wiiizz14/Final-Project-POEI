import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Category} from "../../interfaces/category";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {CategoryService} from "../../services/category.service";

@Component({
  selector: 'app-form-category',
  templateUrl: './form-category.component.html',
  styleUrls: ['./form-category.component.css']
})
export class FormCategoryComponent implements OnInit {

  @Input() mode!: 'new' | 'update';
  @Output() eventForm = new EventEmitter();

  categoryForm!: FormGroup;
  id!: number;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private categoryService: CategoryService,
    private route: ActivatedRoute,
    private routeRedir: Router
  ) {
  }

  get field() {
    return this.categoryForm.controls;
  }

  ngOnInit(): void {
    this.categoryForm = this.formBuilder.group({
      name: ['', Validators.required]
    });

    if (this.mode === 'update') {
      this.id = this.route.snapshot.params['categoryId'];

      this.categoryService
        .getCategoryById(this.id)
        .subscribe((x) => this.categoryForm.patchValue(x));
    }
  }

  onSubmit() {
    this.submitted = true;
    let category: Category = this.categoryForm.value;

    if (this.categoryForm.valid) {
      category.id = this.id;
      this.eventForm.emit(category);
      this.categoryForm.reset();

      if (this.mode === 'new') {
        window.alert('Category created !');
      } else {
        window.alert('Category updated');
      }

      this.routeRedir.navigate(['home']).then(() => location.reload());

    } else {
      window.alert('Form is invalid');
      return;
    }
  }
}
