import {Component, OnInit} from '@angular/core';
import {Category} from "../../interfaces/category";
import {Router} from "@angular/router";
import {CategoryService} from "../../services/category.service";
import {AuthenticationService} from "../../services/helper/authentication.service";

@Component({
  selector: 'app-create-category',
  templateUrl: './create-category.component.html',
  styleUrls: ['./create-category.component.css']
})
export class CreateCategoryComponent implements OnInit {

  isAdmin: boolean = false;

  constructor(
    private route: Router,
    private authenticationService: AuthenticationService,
    private categoryService: CategoryService) {
  }

  ngOnInit(): void {
   this.isAdmin = this.authenticationService.getIsAdmin();

    if (!this.isAdmin) {
      window.alert("You are not administrator !");
      this.route.navigate(['login']);
    }
  }

  onSubmit(categoryToCreate: Category) {
    this.categoryService.createCategory(categoryToCreate).subscribe();
  }
}
