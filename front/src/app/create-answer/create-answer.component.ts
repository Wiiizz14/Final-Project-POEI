import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {AnswerService} from "../../services/answer.service";
import {Answer} from "../../interfaces/answer";

@Component({
  selector: 'app-create-answer',
  templateUrl: './create-answer.component.html',
  styleUrls: ['./create-answer.component.css']
})
export class CreateAnswerComponent implements OnInit {

  constructor(
    private route: Router,
    private answerService: AnswerService) {
  }

  ngOnInit(): void {
  }

  onSubmit(answerToCreate: Answer) {
    this.answerService.createAnswer(answerToCreate).subscribe();
  }
}
