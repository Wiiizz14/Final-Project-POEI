import {ComponentFixture, TestBed} from '@angular/core/testing';

import {GetQuestionsUserComponent} from './get-questions-user.component';

describe('GetQuestionsUserComponent', () => {
  let component: GetQuestionsUserComponent;
  let fixture: ComponentFixture<GetQuestionsUserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [GetQuestionsUserComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GetQuestionsUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
