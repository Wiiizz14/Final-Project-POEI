import {Component, OnInit} from '@angular/core';
import {map} from "rxjs/operators";
import {Router} from "@angular/router";
import {AuthenticationService} from "../../services/helper/authentication.service";
import {QuestionService} from "../../services/question.service";
import {CategoryService} from "../../services/category.service";
import {StudentService} from "../../services/student.service";
import {Observable} from "rxjs";
import {Question} from "../../interfaces/question";
import {Category} from "../../interfaces/category";

@Component({
  selector: 'app-get-questions-user',
  templateUrl: './get-questions-user.component.html',
  styleUrls: ['./get-questions-user.component.css']
})
export class GetQuestionsUserComponent implements OnInit {

  obsQuestions!: Observable<Question[]>;
  obsCategories!: Observable<Category[]>;

  currentUserId!: number;
  username!: string | void;

  constructor(
    private route: Router,
    private authenticationService: AuthenticationService,
    private questionService: QuestionService,
    private categoryService: CategoryService,
    private studentService: StudentService) {
  }

  ngOnInit(): void {
    //Get currentUserId
    this.username = this.authenticationService.getCurrentUserName();
    this.studentService.getStudentByUsername(this.username).subscribe((student) => {
      this.currentUserId = student.id

      //Get all questions drafts and sort by id
      this.obsQuestions = this.questionService.getQuestionsByStatusAndStudentId('DRAFT', this.currentUserId)
        .pipe(map((items) => {
          items.sort((a, b) => {
            return a.id < b.id ? -1 : 1;
          });
          return items;
        }));
    });

    //Get all categories
    this.obsCategories = this.categoryService.getCategories();
  }

  onQuestionDetailsClick(id: number) {
    this.route.navigate(['questions/' + id]);
  }

  onQuestionUpdateClick(id: number) {
    this.route.navigate(['questions/' + id + '/update']);
  }

  onQuestionPublishedClick(id: number) {
    this.questionService.getQuestionById(id).subscribe((question) => {
      question.status = "PUBLISHED";
      this.questionService.updateQuestion(question).subscribe();
      this.route.navigate(['questions/']).then(() => location.reload());
    });
  }
}
