import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";
import {FormLoginComponent} from './form-login/form-login.component';
import {BasicAuthInterceptorService} from "../services/helper/basic-auth-interceptor.service";
import {ErrorInterceptor} from "../services/helper/ErrorInterceptor";
import {GetHomepageComponent} from './get-homepage/get-homepage.component';
import {GetNavbarComponent} from './get-navbar/get-navbar.component';
import {GetCategoriesComponent} from './get-categories/get-categories.component';
import {GetCategoryComponent} from './get-category/get-category.component';
import {FormCategoryComponent} from './form-category/form-category.component';
import {CreateCategoryComponent} from './create-category/create-category.component';
import {UpdateCategoryComponent} from './update-category/update-category.component';
import {GetQuestionsComponent} from './get-questions/get-questions.component';
import {GetQuestionComponent} from './get-question/get-question.component';
import {FormQuestionComponent} from './form-question/form-question.component';
import {CreateQuestionComponent} from './create-question/create-question.component';
import {UpdateQuestionComponent} from './update-question/update-question.component';
import {FormAnswerComponent} from './form-answer/form-answer.component';
import {CreateAnswerComponent} from './create-answer/create-answer.component';
import {UpdateAnswerComponent} from './update-answer/update-answer.component';
import {GetQuestionsUserComponent} from './get-questions-user/get-questions-user.component';

@NgModule({
  declarations: [
    AppComponent,
    FormLoginComponent,
    GetHomepageComponent,
    GetNavbarComponent,
    GetCategoryComponent,
    GetCategoriesComponent,
    FormCategoryComponent,
    CreateCategoryComponent,
    UpdateCategoryComponent,
    GetQuestionsComponent,
    GetQuestionComponent,
    FormQuestionComponent,
    CreateQuestionComponent,
    UpdateQuestionComponent,
    FormAnswerComponent,
    CreateAnswerComponent,
    UpdateAnswerComponent,
    GetQuestionsUserComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      {path: '', component: FormLoginComponent},
      {path: 'home', component: GetHomepageComponent},
      {path: 'categories', component: GetCategoriesComponent},
      {path: 'categories/new', component: CreateCategoryComponent},
      {path: 'categories/:id/update', component: UpdateCategoryComponent},
      {path: 'categories/:id', component: GetCategoryComponent},
      {path: 'questions', component: GetQuestionsComponent},
      {path: 'questions/user', component: GetQuestionsUserComponent},
      {path: 'questions/new', component: CreateQuestionComponent},
      {path: 'questions/:questionId/update', component: UpdateQuestionComponent},
      {path: 'questions/:questionId', component: GetQuestionComponent},
      {path: 'questions/:questionId/answers', component: CreateAnswerComponent},
      {path: 'questions/:questionId/answers/:answerId', component: UpdateAnswerComponent},
    ])
  ],
  providers: [{provide: HTTP_INTERCEPTORS, useClass: BasicAuthInterceptorService, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true}],

  bootstrap: [AppComponent]
})
export class AppModule {
}
