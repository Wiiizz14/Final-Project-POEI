import {Component, OnInit} from '@angular/core';
import {QuestionService} from "../../services/question.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Question} from "../../interfaces/question";

@Component({
  selector: 'app-update-question',
  templateUrl: './update-question.component.html',
  styleUrls: ['./update-question.component.css']
})
export class UpdateQuestionComponent implements OnInit {

  id!: number;

  constructor(
    private route: ActivatedRoute,
    private routeRedir: Router,
    private questionService: QuestionService
  ) {
  }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['questionId'];
  }

  onSubmit(questionToUpdate: Question) {
    questionToUpdate.id = this.id;
    this.questionService.updateQuestion(questionToUpdate).subscribe();
  }
}
