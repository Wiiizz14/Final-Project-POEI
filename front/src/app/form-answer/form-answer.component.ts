import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {AnswerService} from "../../services/answer.service";
import {Answer} from "../../interfaces/answer";
import {StudentService} from "../../services/student.service";
import {AuthenticationService} from "../../services/helper/authentication.service";

@Component({
  selector: 'app-form-answer',
  templateUrl: './form-answer.component.html',
  styleUrls: ['./form-answer.component.css']
})
export class FormAnswerComponent implements OnInit {

  @Input() mode!: 'new' | 'update';
  @Output() eventForm = new EventEmitter();

  answerForm!: FormGroup;
  questionId!: number;
  answerId!: number;
  submitted = false;

  currentUserId!: number;
  username!: string | void;

  constructor(
    private formBuilder: FormBuilder,
    private answerService: AnswerService,
    private route: ActivatedRoute,
    private routeRedir: Router,
    private authenticationService: AuthenticationService,
    private studentService: StudentService
  ) {
  }

  get field() {
    return this.answerForm.controls;
  }

  ngOnInit(): void {
    this.answerForm = this.formBuilder.group({
      content: ['', Validators.required]
    });

    this.questionId = this.route.snapshot.params['questionId'];

    if (this.mode === 'update') {
      this.answerId = this.route.snapshot.params['answerId'];

      this.answerService
        .getAnswerById(this.answerId)
        .subscribe((x) => this.answerForm.patchValue(x));
    }

    //Get currentUserId
    this.username = this.authenticationService.getCurrentUserName();
    this.studentService.getStudentByUsername(this.username).subscribe((student) => this.currentUserId = student.id);
  }

  onSubmit() {
    this.submitted = true;
    let answer: Answer = this.answerForm.value;

    if (this.answerForm.valid) {

      answer.id = this.answerId;
      answer.questionId = Number(this.questionId);
      answer.studentId = this.currentUserId;
      answer.vote = 0;

      this.eventForm.emit(answer);

      if (this.mode === 'new') {
        window.alert('Answer created !');
      } else {
        window.alert('Answer updated');
      }

      this.routeRedir.navigate(['questions/' + this.questionId]).then(() => location.reload());

    } else {
      window.alert('Form is invalid');
      return;
    }
  }
}
