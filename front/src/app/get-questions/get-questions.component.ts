import {Component, OnInit} from '@angular/core';
import {Category} from "../../interfaces/category";
import {Question} from "../../interfaces/question";
import {QuestionService} from "../../services/question.service";
import {CategoryService} from "../../services/category.service";
import {Router} from "@angular/router";
import {AuthenticationService} from "../../services/helper/authentication.service";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";
import {StudentService} from "../../services/student.service";

@Component({
  selector: 'app-get-questions',
  templateUrl: './get-questions.component.html',
  styleUrls: ['./get-questions.component.css']
})
export class GetQuestionsComponent implements OnInit {

  obsQuestions!: Observable<Question[]>;
  obsCategories!: Observable<Category[]>;

  currentUserId!: number;
  username!: string | void;

  status: string = 'PUBLISHED';

  constructor(
    private route: Router,
    private authenticationService: AuthenticationService,
    private questionService: QuestionService,
    private categoryService: CategoryService,
    private studentService: StudentService) {
  }

  ngOnInit(): void {
    //Get all questions published and sort by id
    this.obsQuestions = this.questionService.getQuestionByStatus(this.status).pipe(map((items) => {
      items.sort((a, b) => {
        return a.id < b.id ? -1 : 1;
      });
      return items;
    }));

    //Get all categories
    this.obsCategories = this.categoryService.getCategories();

    //Get currentUserId
    this.username = this.authenticationService.getCurrentUserName();
    this.studentService.getStudentByUsername(this.username).subscribe((student) => this.currentUserId = student.id);
  }

  onQuestionDetailsClick(id: number) {
    this.route.navigate(['questions/' + id]);
  }

  onQuestionUpdateClick(id: number) {
    this.route.navigate(['questions/' + id + '/update']);
  }
}
