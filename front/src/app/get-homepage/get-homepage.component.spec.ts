import {ComponentFixture, TestBed} from '@angular/core/testing';

import {GetHomepageComponent} from './get-homepage.component';

describe('GetHomepageComponent', () => {
  let component: GetHomepageComponent;
  let fixture: ComponentFixture<GetHomepageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [GetHomepageComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GetHomepageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
