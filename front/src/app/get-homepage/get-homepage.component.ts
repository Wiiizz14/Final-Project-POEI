import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {map} from "rxjs/operators";

@Component({
  selector: 'app-get-homepage',
  templateUrl: './get-homepage.component.html',
  styleUrls: ['./get-homepage.component.css']
})
export class GetHomepageComponent implements OnInit {

  url: string = environment.apiUrl;
  message!: string;

  constructor(private httpRequest: HttpClient) {
    this.httpRequest = httpRequest
  }

  ngOnInit(): void {
  }

  private getTestWithApi() {
    //Test call from api
    this.httpRequest
      .get(this.url + "/api/test", {responseType: 'text'})
      .pipe(map((response) => {
        this.message = response.toString();
        console.log(this.message);
      })).subscribe();
  }
}
