import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {User} from "../../interfaces/user";
import {AuthenticationService} from "../../services/helper/authentication.service";

@Component({
  selector: 'app-form-login',
  templateUrl: './form-login.component.html',
  styleUrls: ['./form-login.component.css']
})
export class FormLoginComponent implements OnInit {

  @Output() eventForm = new EventEmitter();

  checkoutForm!: FormGroup;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService) {
  }

  get field() {
    return this.checkoutForm.controls;
  }

  ngOnInit(): void {
    this.checkoutForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  onSubmit() {
    let user!: User;

    user = this.checkoutForm.value;
    this.submitted = true;

    if (this.checkoutForm.valid) {
      this.authenticationService.login(user);
    }
  }
}
