import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Answer} from "../../interfaces/answer";
import {AnswerService} from "../../services/answer.service";

@Component({
  selector: 'app-update-answer',
  templateUrl: './update-answer.component.html',
  styleUrls: ['./update-answer.component.css']
})
export class UpdateAnswerComponent implements OnInit {

  questionId!: number;
  answerId!: number;

  constructor(
    private route: ActivatedRoute,
    private routeRedir: Router,
    private answerService: AnswerService) {
  }

  ngOnInit(): void {
    this.questionId = this.route.snapshot.params['questionId'];
    this.answerId = this.route.snapshot.params['answerId'];
  }

  onSubmit(answerToUpdate: Answer) {
    answerToUpdate.id = this.answerId;
    this.answerService.updateAnswer(this.questionId, answerToUpdate).subscribe();
  }
}
