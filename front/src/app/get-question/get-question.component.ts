import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {AuthenticationService} from "../../services/helper/authentication.service";
import {QuestionService} from "../../services/question.service";
import {CategoryService} from "../../services/category.service";
import {Question} from "../../interfaces/question";
import {Category} from "../../interfaces/category";
import {StudentService} from "../../services/student.service";
import {Answer} from "../../interfaces/answer";
import {AnswerService} from "../../services/answer.service";
import {Student} from "../../interfaces/student";
import {Observable} from "rxjs";

@Component({
  selector: 'app-get-question',
  templateUrl: './get-question.component.html',
  styleUrls: ['./get-question.component.css']
})
export class GetQuestionComponent implements OnInit {

  question!: Observable<Question>;
  categories!: Observable<Category[]>;
  students!: Observable<Student[]>;

  answers!: Answer[];
  currentUserId!: number;
  username!: string | void;

  constructor(
    private route: ActivatedRoute,
    private routeRedir: Router,
    private authenticationService: AuthenticationService,
    private questionService: QuestionService,
    private categoryService: CategoryService,
    private studentService: StudentService,
    private answerService: AnswerService
  ) {
  }

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    const questionIdFromRoute = Number(routeParams.get("questionId"));

    //Get question
    this.question = this.questionService.getQuestionById(questionIdFromRoute);

    //Get categories
    this.categories = this.categoryService.getCategories();

    //Get students
    this.students = this.studentService.getStudents();

    //Get username
    this.username = this.authenticationService.getCurrentUserName();

    //Get current userId
    this.studentService.getStudentByUsername(this.username).subscribe((student) => this.currentUserId = student.id);
  }

  onQuestionUpdateClick(id: number) {
    this.routeRedir.navigate(['questions/' + id + '/update']);
  }

  onQuestionResponseClick() {
    let questionId = this.route.snapshot.params['questionId'];
    this.routeRedir.navigate(['questions/' + questionId + '/answers']);
  }

  onAnswerUpdateClick(answerId: number) {
    let questionId = this.route.snapshot.params['questionId'];
    this.routeRedir.navigate(['questions/' + questionId + '/answers/' + answerId]);
  }

  onQuestionPublishedClick(id: number) {
    this.questionService.getQuestionById(id).subscribe((question) => {
      question.status = "PUBLISHED";
      this.questionService.updateQuestion(question).subscribe();
      this.routeRedir.navigate(['questions/']).then(() => location.reload());
    });
  }

  onVoteClick(answerId: number, type: string) {
    let questionId = this.route.snapshot.params['questionId'];
    this.answerService.getAnswerById(answerId).subscribe(() => {
      this.answerService.updateVote(questionId, answerId, type).subscribe(() => location.reload());
    });
  }
}
