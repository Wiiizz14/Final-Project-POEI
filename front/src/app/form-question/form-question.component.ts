import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Observable} from "rxjs";
import {Category} from "../../interfaces/category";
import {ActivatedRoute, Router} from "@angular/router";
import {CategoryService} from "../../services/category.service";
import {QuestionService} from "../../services/question.service";
import {map} from "rxjs/operators";
import {Question} from "../../interfaces/question";
import {AuthenticationService} from "../../services/helper/authentication.service";
import {StudentService} from "../../services/student.service";

@Component({
  selector: 'app-form-question',
  templateUrl: './form-question.component.html',
  styleUrls: ['./form-question.component.css']
})
export class FormQuestionComponent implements OnInit {

  @Input() mode!: 'new' | 'update';
  @Output() eventForm = new EventEmitter();

  categories!: Observable<Category[]>;

  questionForm!: FormGroup;
  questionId!: number;
  currentUserId!: number;
  submitted = false;

  constructor(
    private route: ActivatedRoute,
    private routeRedir: Router,
    private formBuilder: FormBuilder,
    private questionService: QuestionService,
    private categoryService: CategoryService,
    private authenticationService: AuthenticationService,
    private studentService: StudentService
  ) {
  }

  get field() {
    return this.questionForm.controls;
  }


  ngOnInit(): void {
    this.questionForm = this.formBuilder.group({
      categoryId: ['', Validators.required],
      title: ['', Validators.required],
      content: ['', Validators.required]
    });

    //Get category list for selection
    this.categories = this.categoryService.getCategories().pipe(map((items) => {
      items.sort((a, b) => {
        return a.id < b.id ? -1 : 1;
      });
      return items;
    }));

    //Check mode
    if (this.mode === 'update') {
      this.questionId = this.route.snapshot.params['questionId'];

      this.questionService
        .getQuestionById(this.questionId)
        .subscribe((x) => this.questionForm.patchValue(x));
    }

    //Get currentUserId
    let username = this.authenticationService.getCurrentUserName();
    this.studentService.getStudentByUsername(username).subscribe((student) => this.currentUserId = student.id);
  }

  onSubmit() {
    this.submitted = true;
    let question: Question = this.questionForm.value;
    question.status = "DRAFT";

    if (this.questionForm.valid) {

      question.id = this.questionId;
      question.studentId = this.currentUserId;

      this.eventForm.emit(question);
      this.questionForm.reset();

      if (this.mode === 'new') {
        window.alert('Question created !');
      } else {
        window.alert('Question updated');
      }

      this.routeRedir.navigate(['questions']);

    } else {
      window.alert('Form is invalid');
      return;
    }
  }
}
