import {Component, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {Category} from "../../interfaces/category";
import {ActivatedRoute, Router} from "@angular/router";
import {CategoryService} from "../../services/category.service";
import {map} from "rxjs/operators";
import {StudentService} from "../../services/student.service";
import {AuthenticationService} from "../../services/helper/authentication.service";

@Component({
  selector: 'app-get-category',
  templateUrl: './get-category.component.html',
  styleUrls: ['./get-category.component.css']
})
export class GetCategoryComponent implements OnInit {

  category!: Observable<Category>;

  currentUserId!: number;
  username!: string | void;

  constructor(
    private route: ActivatedRoute,
    private routeRedir: Router,
    private authenticationService: AuthenticationService,
    private categoryService: CategoryService,
    private studentService: StudentService
  ) {
  }

  ngOnInit(): void {
    this.route.params.subscribe(routeParam => {
      this.category = this.categoryService.getCategoryById(routeParam.id).pipe(map((category) => {
        category.questions.sort((a, b) => {
          return a.id < b.id ? -1 : 1;
        });
        return category;
      }));
    });

    //Get currentUserId
    this.username = this.authenticationService.getCurrentUserName();
    this.studentService.getStudentByUsername(this.username).subscribe((student) => this.currentUserId = student.id);
  }

  onQuestionDetailsClick(id: number) {
    this.routeRedir.navigate(['questions/' + id]);
  }

  onQuestionUpdateClick(id: number) {
    this.routeRedir.navigate(['questions/' + id + '/update']);
  }
}
