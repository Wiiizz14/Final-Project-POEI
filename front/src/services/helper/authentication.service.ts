import {Injectable} from '@angular/core';
import {User} from "../../interfaces/user";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private readonly SESSION_STORAGE_KEY = "currentUser";

  constructor(private routeRedir: Router) {
  }

  login(user: User) {
    sessionStorage.setItem(this.SESSION_STORAGE_KEY, JSON.stringify(user));
    console.log("Login OK");
    this.routeRedir.navigate(['home']);
  }

  logout(): void {
    sessionStorage.removeItem(this.SESSION_STORAGE_KEY);
    this.warnLoggedOut();
    this.routeRedir.navigate(['']);
  }

  getCurrentUserBasicAuthentication(): string | undefined {
    const currentUserPlain = sessionStorage.getItem(this.SESSION_STORAGE_KEY);
    if (currentUserPlain) {
      const currentUser: User = JSON.parse(currentUserPlain);
      return "Basic " + btoa(currentUser.username + ":" + currentUser.password);
    } else {
      return undefined;
    }
  }

  getCurrentUserName() {
    const currentUserPlain = sessionStorage.getItem(this.SESSION_STORAGE_KEY);
    if (currentUserPlain) {
      const currentUser: User = JSON.parse(currentUserPlain);
      return currentUser.username;
    }
    return console.warn("Username not found !");
  }

  getIsAdmin(): boolean {
    let auth = this.getCurrentUserBasicAuthentication();
    let isAdm = false;

    if (auth === "Basic QWxleGFuZHJlOmFkbWlu") {
      isAdm = true;
    }
    return isAdm;
  }

  warnLoggedOut(): void {
    window.alert("You are deconnected !");
  }
}
