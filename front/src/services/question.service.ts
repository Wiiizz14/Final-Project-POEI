import {Injectable} from '@angular/core';
import {environment} from "../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Question} from "../interfaces/question";

@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  url: string = environment.apiUrl;

  constructor(private httpRequest: HttpClient) {
    this.httpRequest = httpRequest;
  }

  createQuestion(question: Question) {
    return this.httpRequest.post<Question>(this.url + "/api/questions", question);
  }

  getQuestions() {
    return this.httpRequest.get<Question[]>(this.url + "/api/questions");
  }

  getQuestionById(id: number) {
    return this.httpRequest.get<Question>(this.url + "/api/questions/" + id);
  }

  getQuestionByStatus(status: string) {
    return this.httpRequest.get<Question[]>(this.url + "/api/questions/?status=" + status);
  }

  getQuestionsByStatusAndStudentId(status: string, studentId: number) {
    return this.httpRequest.get<Question[]>(this.url + "/api/questions/user/" + studentId + "?status=" + status);
  }

  updateQuestion(question: Question) {
    return this.httpRequest.put<Question>(this.url + "/api/questions/" + question.id, question);
  }
}
