import {Injectable} from '@angular/core';
import {environment} from "../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Student} from "../interfaces/student";

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  url: string = environment.apiUrl;

  constructor(private httpRequest: HttpClient) {
    this.httpRequest = httpRequest;
  }

  getStudents(){
    return this.httpRequest.get<Student[]>(this.url + "/api/students");
  }

  getStudentById(id: number) {
    return this.httpRequest.get<Student>(this.url + "/api/students/" + id);
  }

  getStudentByUsername(username: string | void) {
    return this.httpRequest.get<Student>(this.url + "/api/students/?username=" + username);
  }
}
