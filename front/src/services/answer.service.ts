import {Injectable} from '@angular/core';
import {environment} from "../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Answer} from "../interfaces/answer";

@Injectable({
  providedIn: 'root'
})
export class AnswerService {

  url: string = environment.apiUrl;

  constructor(private httpRequest: HttpClient) {
    this.httpRequest = httpRequest;
  }

  getAnswerById(id: number) {
    return this.httpRequest.get<Answer>(this.url + "/api/answers/" + id);
  }

  createAnswer(answer: Answer) {
    return this.httpRequest.post<Answer>(this.url + "/api/questions/" + answer.questionId + "/answers", answer);
  }

  updateAnswer(questionId: number, answer: Answer) {
    return this.httpRequest.put<Answer>(this.url + "/api/questions/" + questionId + "/answers/" + answer.id, answer);
  }

  updateVote(questionId: number, answerId: number, type: string) {
    return this.httpRequest.put<Answer>(this.url + "/api/questions/" + questionId + "/answers/" + answerId + "/vote/" + type, null);
  }
}
