export interface Answer {

  id: number,
  questionId: number,
  studentId: number,
  content: string,
  vote: number
}
