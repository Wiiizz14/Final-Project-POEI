import {Answer} from "./answer";

export interface Question {

  id: number
  categoryId: number
  studentId: number
  status: string
  title: string
  content: string
  answers: Answer[]
}
