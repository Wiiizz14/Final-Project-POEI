import {Question} from "./question";
import {Answer} from "./answer";

export interface Student {

  id: number,
  username: string,
  email: string
  questions: Question[]
  answers: Answer[]
}
