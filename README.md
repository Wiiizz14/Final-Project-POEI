<h1 align="center">Welcome to Final Project POEI 👋</h1>
<p>
</p>

## Description

Site de questions / réponses de la Zenika Academy -QueueUnderStream

L’outil présentera des fonctionnalités similaires à une platforme de questions / réponses existantes
sur Internet : StackOverFlow.
L’application permettra aux élèves de l’actuelle promotion de poser des questions à ceux des
précédentes promotions.
Afin de faciliter la recherche des questions, un système de catégorie est à prévoir sur les questions.

## Build

Linux users :

```sh
./build.sh
```

Windows users :

```ps1
./build.ps1
```

## Start

```sh
docker-compose up
```

## Author

👤 **Thibault James**

## Show your support

Give a ⭐️ if this project helped you!
