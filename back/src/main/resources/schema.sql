--SECURE DROP
DROP TABLE IF EXISTS answers, questions, categories, students;

--CREATE TABLES
CREATE TABLE IF NOT EXISTS students
(
    id       INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    username TEXT NOT NULL UNIQUE,
    email    TEXT NOT NULL UNIQUE
);

CREATE TABLE IF NOT EXISTS categories
(
    id   INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    name TEXT NOT NULL UNIQUE
);

CREATE TABLE IF NOT EXISTS questions
(
    id            INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    title         TEXT NOT NULL,
    content       TEXT NOT NULL,
    status        TEXT NOT NULL DEFAULT 'DRAFT',
    categories_id INTEGER REFERENCES categories (id),
    students_id   INTEGER REFERENCES students (id)
);

CREATE TABLE IF NOT EXISTS answers
(
    id           INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    questions_id INTEGER REFERENCES questions (id),
    students_id  INTEGER REFERENCES students (id),
    content      TEXT    NOT NULL,
    vote         INTEGER NOT NULL DEFAULT 0
);

--DATA SAMPLES CATEGORIES
INSERT INTO categories
VALUES (DEFAULT, 'Laptops');
INSERT INTO categories
VALUES (DEFAULT, 'Desktops');
INSERT INTO categories
VALUES (DEFAULT, 'Monitors');
INSERT INTO categories
VALUES (DEFAULT, 'Servers');
INSERT INTO categories
VALUES (DEFAULT, 'Accessories');

--DATA SAMPLES STUDENTS
INSERT INTO students
VALUES (DEFAULT, 'Thibault', 'thib.test@mail.fr');
INSERT INTO students
VALUES (DEFAULT, 'Alexandre', 'alex.test@mail.fr');

--DATA SAMPLES QUESTIONS
INSERT INTO questions
VALUES (DEFAULT, 'What is the best laptop ?', 'Help me to find the best laptop !', DEFAULT, 1, 1);
INSERT INTO questions
VALUES (DEFAULT, 'What is the lowest laptop for studying ?', 'I have no money !', DEFAULT, 1, 1);
INSERT INTO questions
VALUES (DEFAULT, 'Where to buy a laptop ?', 'I want to buy it now !', 'PUBLISHED', 1, 1);
INSERT INTO questions
VALUES (DEFAULT, 'Why my desktop burned ?', 'I like to feel the heat of my hard drive ', 'PUBLISHED', 2, 2);
INSERT INTO questions
VALUES (DEFAULT, 'Who stole my desktop ?', 'I had left the window open', 'PUBLISHED', 2, 2);

--DATA SAMPLES ANSWERS
INSERT INTO answers
VALUES (DEFAULT, 3, 2, 'TestContent', 0);
INSERT INTO answers
VALUES (DEFAULT, 3, 1, 'TestContentGoodResponse', 1);
INSERT INTO answers
VALUES (DEFAULT, 4, 1, 'TestContent', 0);
INSERT INTO answers
VALUES (DEFAULT, 4, 2, 'TestContentBadResponse', -1);
