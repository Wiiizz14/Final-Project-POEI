package com.example.back.repositories;

import com.example.back.models.Answer;
import org.springframework.data.jpa.repository.JpaRepository;


public interface AnswerRepo extends JpaRepository<Answer, Integer> {
}