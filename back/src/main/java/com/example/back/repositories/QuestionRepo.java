package com.example.back.repositories;

import com.example.back.models.Question;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface QuestionRepo extends JpaRepository<Question, Integer> {

    Set<Question> getQuestionByCategoryId(Integer id);

    Set<Question> findAllByStatusEqualsAndStudentId(String status, Integer studentId);

    Set<Question> findAllByStatusEquals(String status);
}