package com.example.back.repositories;

import com.example.back.models.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface StudentRepo extends JpaRepository<Student, Integer> {

    Optional<Student> getStudentByUsername(String username);
}
