package com.example.back.controllers.dtos;

import com.example.back.models.Category;
import lombok.Data;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Data
public class CategoryDto {

    private Integer id;
    private String name;
    private Set<QuestionDto> questions = new HashSet<>();

    public static CategoryDto fromDomain(Category category) {
        CategoryDto categoryDto = new CategoryDto();

        categoryDto.setId(category.getId());
        categoryDto.setName(category.getName());
        categoryDto.setQuestions(category.getQuestions()
                .stream()
                .map(QuestionDto::fromDomain)
                .collect(Collectors.toSet()));

        return categoryDto;
    }

    public Category toDomain() {
        Category category = new Category();
        category.setId(this.id);
        category.setName(this.name);

        category.setQuestions(this.getQuestions()
                .stream()
                .map(QuestionDto::toDomain)
                .collect(Collectors.toSet()));

        return category;
    }
}
