package com.example.back.controllers.dtos;

import com.example.back.models.Student;
import lombok.Data;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Data
public class StudentDto {

    private Integer id;
    private String username;
    private String email;
    private Set<QuestionDto> questions = new HashSet<>();
    private Set<AnswerDto> answers = new HashSet<>();

    public static StudentDto fromDomain(Student student) {
        StudentDto studentDto = new StudentDto();

        studentDto.setId(student.getId());
        studentDto.setUsername(student.getUsername());
        studentDto.setEmail(student.getEmail());

        studentDto.setQuestions(student.getQuestions()
                .stream()
                .map(QuestionDto::fromDomain)
                .collect(Collectors.toSet()));

        studentDto.setAnswers(student.getAnswers()
                .stream()
                .map(AnswerDto::fromDomain)
                .collect(Collectors.toSet()));

        return studentDto;
    }

    public static StudentDto fromDomainLight(Student student) {
        StudentDto studentDto = new StudentDto();

        studentDto.setId(student.getId());
        studentDto.setUsername(student.getUsername());
        studentDto.setEmail(student.getEmail());

        return studentDto;
    }

    public Student toDomain() {
        Student student = new Student();

        student.setId(this.id);
        student.setUsername(this.username);
        student.setEmail(this.email);

        student.setQuestions(this.getQuestions()
                .stream()
                .map(QuestionDto::toDomain)
                .collect(Collectors.toSet()));

        student.setAnswers(this.getAnswers()
                .stream()
                .map(AnswerDto::toDomain)
                .collect(Collectors.toSet()));

        return student;
    }
}
