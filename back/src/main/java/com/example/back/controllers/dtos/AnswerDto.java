package com.example.back.controllers.dtos;

import com.example.back.models.Answer;
import com.example.back.models.Question;
import com.example.back.models.Student;
import lombok.Data;

@Data
public class AnswerDto {

    private Integer id;
    private Integer questionId;
    private Integer studentId;
    private String content;
    private Integer vote;

    public static AnswerDto fromDomain(Answer answer) {
        AnswerDto answerDto = new AnswerDto();

        answerDto.setId(answer.getId());
        answerDto.setQuestionId(QuestionDto.fromDomainLight(answer.getQuestion()).getId());
        answerDto.setStudentId(StudentDto.fromDomainLight(answer.getStudent()).getId());
        answerDto.setContent(answer.getContent());
        answerDto.setVote(answer.getVote());

        return answerDto;
    }

    public Answer toDomain() {
        Answer answer = new Answer();
        answer.setId(this.id);

        Question question = new Question();
        question.setId(this.questionId);
        answer.setQuestion(question);

        Student student = new Student();
        student.setId(this.studentId);
        answer.setStudent(student);

        answer.setContent(this.content);
        answer.setVote(this.getVote());

        return answer;
    }
}
