package com.example.back.controllers.dtos;

import com.example.back.models.Category;
import com.example.back.models.Question;
import com.example.back.models.Student;
import lombok.Data;

import java.util.Set;
import java.util.stream.Collectors;

@Data
public class QuestionDto {

    private Integer id;
    private Integer categoryId;
    private Integer studentId;
    private String status;
    private String title;
    private String content;
    private Set<AnswerDto> answers;

    public static QuestionDto fromDomain(Question question) {
        QuestionDto questionDto = new QuestionDto();

        questionDto.setId(question.getId());
        questionDto.setCategoryId(question.getCategory().getId());
        questionDto.setStudentId(question.getStudent().getId());
        questionDto.setStatus(question.getStatus());
        questionDto.setTitle(question.getTitle());
        questionDto.setContent(question.getContent());

        questionDto.setAnswers(question.getAnswerSet()
                .stream()
                .map(AnswerDto::fromDomain)
                .collect(Collectors.toSet()));

        return questionDto;
    }

    public static QuestionDto fromDomainLight(Question question) {
        QuestionDto questionDto = new QuestionDto();

        questionDto.setId(question.getId());
        questionDto.setCategoryId(question.getCategory().getId());
        questionDto.setStudentId(question.getStudent().getId());
        questionDto.setStatus(question.getStatus());
        questionDto.setTitle(question.getTitle());
        questionDto.setContent(question.getContent());

        return questionDto;
    }

    public Question toDomain() {
        Question question = new Question();
        question.setId(this.id);
        question.setStatus(this.status);

        Category category = new Category();
        category.setId(categoryId);
        question.setCategory(category);

        Student student = new Student();
        student.setId(studentId);
        question.setStudent(student);

        question.setTitle(this.title);
        question.setContent(this.content);

        question.setAnswerSet(this.getAnswers()
                .stream()
                .map(AnswerDto::toDomain)
                .collect(Collectors.toSet()));

        return question;
    }

    public Question toDomainLight() {
        Question question = new Question();
        question.setId(this.id);
        question.setStatus(this.status);

        Category category = new Category();
        category.setId(categoryId);
        question.setCategory(category);

        Student student = new Student();
        student.setId(studentId);
        question.setStudent(student);

        question.setTitle(this.title);
        question.setContent(this.content);

        return question;
    }
}
