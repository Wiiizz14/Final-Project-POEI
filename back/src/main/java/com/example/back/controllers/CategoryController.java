package com.example.back.controllers;

import com.example.back.controllers.dtos.CategoryDto;
import com.example.back.models.Category;
import com.example.back.models.Question;
import com.example.back.models.exceptions.NotFoundException;
import com.example.back.repositories.CategoryRepo;
import com.example.back.repositories.QuestionRepo;
import com.example.back.services.CategoryService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@CrossOrigin
public class CategoryController {

    private final CategoryRepo categoryRepo;
    private final QuestionRepo questionRepo;
    private final CategoryService categoryService;

    public CategoryController(CategoryRepo categoryRepo, QuestionRepo questionRepo, CategoryService categoryService) {
        this.categoryRepo = categoryRepo;
        this.questionRepo = questionRepo;
        this.categoryService = categoryService;
    }

    //Get All
    @GetMapping("/api/categories")
    public Set<CategoryDto> getCategories() {
        List<Category> categories = categoryRepo.findAll();
        Set<CategoryDto> categoriesDto = new HashSet<>();
        for (Category c : categories) {
            Set<Question> questions = questionRepo.getQuestionByCategoryId(c.getId());
            c.setQuestions(questions);
            categoriesDto.add(CategoryDto.fromDomain(c));
        }
        return categoriesDto;
    }

    //Get by name
    @GetMapping("/api/categories/")
    public CategoryDto getCategoryByName(@RequestParam String name) {
        return categoryRepo.getCategoryByName(name)
                .map(CategoryDto::fromDomain)
                .orElseThrow(() -> new NotFoundException("Name does not exist"));
    }

    //Get by id
    @GetMapping("/api/categories/{id}")
    public CategoryDto getCategoryById(@PathVariable("id") Integer id) {
        return categoryRepo.findById(id)
                .map(CategoryDto::fromDomain)
                .orElseThrow(() -> new NotFoundException("Id does not exist"));
    }

    //Create
    @PostMapping("/api/categories")
    public ResponseEntity<Category> createCategory(@RequestBody CategoryDto categoryDto) {
        Category category = categoryService.createCategory(categoryDto.toDomain());
        return ResponseEntity
                .created(URI.create("/api/categories/" + category.getId()))
                .body(category);
    }

    //Update
    @PutMapping("/api/categories/{id}")
    public CategoryDto updateCategory(@PathVariable("id") Integer id, @RequestBody CategoryDto categoryDto) {
        return CategoryDto.fromDomain(categoryService.updateCategory(id, categoryDto));
    }

    //Delete
    @DeleteMapping("/api/categories/{id}")
    public ResponseEntity<String> deleteCategory(@PathVariable("id") Integer id) {
        categoryService.deleteCategory(id);
        return ResponseEntity
                .status(HttpStatus.NO_CONTENT)
                .body("Category deleted");
    }
}
