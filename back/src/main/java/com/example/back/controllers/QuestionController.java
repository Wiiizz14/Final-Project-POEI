package com.example.back.controllers;

import com.example.back.controllers.dtos.AnswerDto;
import com.example.back.controllers.dtos.QuestionDto;
import com.example.back.controllers.dtos.StudentDto;
import com.example.back.models.Answer;
import com.example.back.models.Question;
import com.example.back.models.Student;
import com.example.back.models.exceptions.NotFoundException;
import com.example.back.repositories.AnswerRepo;
import com.example.back.repositories.QuestionRepo;
import com.example.back.repositories.StudentRepo;
import com.example.back.services.QuestionService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@CrossOrigin
public class QuestionController {

    private final QuestionRepo questionRepo;
    private final AnswerRepo answerRepo;
    private final StudentRepo studentRepo;
    private final QuestionService questionService;

    public QuestionController(QuestionRepo questionRepo, QuestionService questionService, AnswerRepo answerRepo, StudentRepo studentRepo) {
        this.questionRepo = questionRepo;
        this.questionService = questionService;
        this.answerRepo = answerRepo;
        this.studentRepo = studentRepo;
    }

    //Get questions
    @GetMapping("/api/questions")
    public Set<QuestionDto> getQuestions() {
        return questionRepo.findAll().stream()
                .map(QuestionDto::fromDomain)
                .collect(Collectors.toSet());
    }

    //Get question by id
    @GetMapping("/api/questions/{id}")
    public QuestionDto getQuestionByid(@PathVariable("id") Integer id) {
        Question question = questionRepo.findById(id)
                .orElseThrow(() -> new NotFoundException("Question does not exist"));
        return QuestionDto.fromDomain(question);
    }

    //Get questions by status
    @GetMapping("/api/questions/")
    public Set<QuestionDto> getAllByStatus(@RequestParam("status") String status) {
        Set<Question> questions = questionRepo.findAllByStatusEquals(status);
        Set<QuestionDto> questionsDto = new HashSet<>();
        for (Question q : questions) {
            questionsDto.add(QuestionDto.fromDomain(q));
        }
        return questionsDto;
    }

    //Get questions by status and user
    @GetMapping("/api/questions/user/{id}")
    public Set<QuestionDto> getAllByStatusAndUser(@PathVariable("id") Integer id, @RequestParam("status") String status) {
        Set<Question> questions = questionRepo.findAllByStatusEqualsAndStudentId(status, id);
        Set<QuestionDto> questionsDto = new HashSet<>();
        for (Question q : questions) {
            questionsDto.add(QuestionDto.fromDomain(q));
        }
        return questionsDto;
    }

    //Create question
    @PostMapping("/api/questions")
    public ResponseEntity<QuestionDto> createQuestion(@RequestBody QuestionDto questionDto) {
        Question q = questionService.createQuestion(questionDto.toDomainLight());
        return ResponseEntity
                .created(URI.create("/api/questions/" + q.getId()))
                .body(QuestionDto.fromDomain(q));
    }

    //Update question
    @PutMapping("/api/questions/{id}")
    public QuestionDto updateQuestion(@PathVariable("id") Integer id, @RequestBody QuestionDto questionDto) {
        return QuestionDto.fromDomain(questionService.updateQuestion(id, questionDto));
    }

    //Get answer by id
    @GetMapping("/api/answers/{id}")
    public AnswerDto getAnswerById(@PathVariable("id") Integer id) {
        Answer answer = answerRepo.findById(id)
                .orElseThrow(() -> new NotFoundException("Answer does not exist"));
        return AnswerDto.fromDomain(answer);
    }

    //Create answer
    @PostMapping("/api/questions/{id}/answers")
    public ResponseEntity<Answer> createAnswer(@PathVariable("id") Integer id, @RequestBody AnswerDto answerDto) {
        Question q = questionRepo.findById(id)
                .orElseThrow(() -> new NotFoundException("Question does not exist"));

        Student s = studentRepo.findById(answerDto.getStudentId())
                .orElseThrow(() -> new NotFoundException("Student does not exist"));

        answerDto.setQuestionId(QuestionDto.fromDomain(q).getId());
        answerDto.setStudentId(StudentDto.fromDomain(s).getId());

        Answer answer = questionService.createAnswer(answerDto.toDomain());

        return ResponseEntity
                .created(URI.create("/api/questions/" + id + "/answers/" + answer.getId()))
                .body(answer);
    }

    //Update answer
    @PutMapping("/api/questions/{qId}/answers/{aId}")
    public AnswerDto updateAnswer(
            @PathVariable("qId") Integer qId,
            @PathVariable("aId") Integer aId,
            @RequestBody AnswerDto answerDto) {

        return AnswerDto.fromDomain(questionService.updateAnswer(qId, aId, answerDto));
    }

    //Check vote
    @PutMapping("/api/questions/{qId}/answers/{aId}/vote/{type}")
    public AnswerDto voteAnswer(
            @PathVariable("qId") Integer qId,
            @PathVariable("aId") Integer aId,
            @PathVariable("type") String typeVote) {

        return AnswerDto.fromDomain((questionService.updateVote(qId, aId, typeVote)));
    }
}