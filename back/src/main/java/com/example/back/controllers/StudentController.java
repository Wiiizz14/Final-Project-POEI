package com.example.back.controllers;

import com.example.back.controllers.dtos.StudentDto;
import com.example.back.models.exceptions.NotFoundException;
import com.example.back.repositories.StudentRepo;
import org.springframework.web.bind.annotation.*;

import java.util.Set;
import java.util.stream.Collectors;

@RestController
@CrossOrigin
public class StudentController {

    private final StudentRepo studentRepo;

    public StudentController(StudentRepo studentRepo) {
        this.studentRepo = studentRepo;
    }

    //Get All
    @GetMapping("/api/students")
    public Set<StudentDto> getStudents() {
        return studentRepo.findAll()
                .stream()
                .map(StudentDto::fromDomain)
                .collect(Collectors.toSet());
    }

    //Get by username
    @GetMapping("/api/students/")
    public StudentDto getStudentById(@RequestParam("username") String username) {
        return studentRepo.getStudentByUsername(username)
                .map(StudentDto::fromDomain)
                .orElseThrow(() -> new NotFoundException("Username does not exist"));
    }

    //Get by id
    @GetMapping("/api/students/{id}")
    public StudentDto getStudentById(@PathVariable("id") Integer id) {
        return studentRepo.findById(id)
                .map(StudentDto::fromDomain)
                .orElseThrow(() -> new NotFoundException("Student does not exist"));
    }
}
