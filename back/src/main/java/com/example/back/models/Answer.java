package com.example.back.models;

import javax.persistence.*;

@Entity
@Table(name = "answers")
public class Answer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "questions_id")
    private Question question;

    @ManyToOne
    @JoinColumn(name = "students_id")
    private Student student;

    private String content;
    private Integer vote;

    public Answer(Question question, Student student, String content, Integer vote) {
        this.question = question;
        this.student = student;
        this.content = content;
        this.vote = vote;
    }

    //For JPA
    @Deprecated
    public Answer() {
    }

    public int addPositiveVote(){
         return vote+=1;
    }

    public int addNegativeVote(){
        return vote-=1;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getVote() {
        return vote;
    }

    public void setVote(Integer vote) {
        this.vote = vote;
    }
}