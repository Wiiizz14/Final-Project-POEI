package com.example.back.models;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "students")
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String username;
    private String email;

    @OneToMany(mappedBy = "student")
    private Set<Question> questions = new HashSet<>();

    @OneToMany(mappedBy = "student")
    private Set<Answer> answers = new HashSet<>();

    public Student(String username, String email, Set<Question> questions, Set<Answer> answers) {
        this.username = username;
        this.email = email;
        this.questions = questions;
        this.answers = answers;
    }

    public Student(String username, String email) {
        this.username = username;
        this.email = email;
    }

    //For JPA
    @Deprecated
    public Student() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(Set<Question> questions) {
        this.questions = questions;
    }

    public Set<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(Set<Answer> answers) {
        this.answers = answers;
    }
}