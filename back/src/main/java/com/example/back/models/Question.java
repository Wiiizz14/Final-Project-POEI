package com.example.back.models;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "questions")
public class Question {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String title;
    private String content;
    private String status;

    @ManyToOne
    @JoinColumn(name = "categories_id")
    private Category category;

    @OneToMany(mappedBy = "question")
    private Set<Answer> answerSet = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "students_id")
    private Student student;

    public Question(String title, String content, Category category, Set<Answer> answerSet, Student student) {
        this.title = title;
        this.content = content;
        this.status = "DRAFT";
        this.category = category;
        this.answerSet = answerSet;
        this.student = student;
    }

    public Question(String title, String content, Category category, Student student) {
        this.title = title;
        this.content = content;
        this.status = "DRAFT";
        this.category = category;
        this.student = student;
    }

    //For JPA
    @Deprecated
    public Question() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Set<Answer> getAnswerSet() {
        return answerSet;
    }

    public void setAnswerSet(Set<Answer> answerSet) {
        this.answerSet = answerSet;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
}