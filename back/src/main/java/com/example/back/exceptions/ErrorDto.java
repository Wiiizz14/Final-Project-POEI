package com.example.back.exceptions;

import lombok.Data;

import java.time.Instant;

@Data
public class ErrorDto {
    private Instant timestamp = Instant.now();
    private String message;

    public ErrorDto() {
    }

    public ErrorDto(String message) {
        this.message = message;
    }
}
