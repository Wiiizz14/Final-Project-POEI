package com.example.back.services;

import com.example.back.controllers.dtos.CategoryDto;
import com.example.back.models.Category;
import com.example.back.models.exceptions.NotFoundException;
import com.example.back.repositories.CategoryRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
public class CategoryService {

    private final CategoryRepo categoryRepo;

    @Transactional
    public Category createCategory(Category category) {
        return categoryRepo.save(category);
    }

    @Transactional
    public Category updateCategory(Integer id, CategoryDto categoryDto) {
        Category categoryToUpdate = categoryRepo.findById(id)
                .orElseThrow(() -> new NotFoundException("Category does not exist"));
        categoryToUpdate.setName(categoryDto.getName());
        return categoryToUpdate;
    }

    @Transactional
    public void deleteCategory(Integer id) {
        Category categoryToRemove = categoryRepo.findById(id)
                .orElseThrow(() -> new NotFoundException("Category does not exist"));
        categoryRepo.delete(categoryToRemove);
    }
}