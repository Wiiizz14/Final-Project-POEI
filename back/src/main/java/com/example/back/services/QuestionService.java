package com.example.back.services;

import com.example.back.controllers.dtos.AnswerDto;
import com.example.back.controllers.dtos.QuestionDto;
import com.example.back.models.Answer;
import com.example.back.models.Category;
import com.example.back.models.Question;
import com.example.back.models.exceptions.FunctionalException;
import com.example.back.models.exceptions.NotFoundException;
import com.example.back.repositories.AnswerRepo;
import com.example.back.repositories.CategoryRepo;
import com.example.back.repositories.QuestionRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
public class QuestionService {

    private final QuestionRepo questionRepo;
    private final CategoryRepo categoryRepo;
    private final AnswerRepo answerRepo;

    @Transactional
    public Question createQuestion(Question question) {
        return questionRepo.save(question);
    }

    @Transactional
    public Question updateQuestion(Integer id, QuestionDto questionDto) {
        Question questionToUpdate = questionRepo.findById(id)
                .orElseThrow(() -> new NotFoundException("Question does not exist"));

        //Prevent published update
        if (questionToUpdate.getStatus().equals("PUBLISHED")) {
            throw new FunctionalException("Cannot update a question published");
        }

        questionToUpdate.setTitle(questionDto.getTitle());
        questionToUpdate.setContent(questionDto.getContent());
        questionToUpdate.setStatus(questionDto.getStatus());

        Category existingCategory = categoryRepo.findById(questionDto.getCategoryId())
                .orElseThrow(() -> new NotFoundException("Category does not exist"));

        questionToUpdate.setCategory(existingCategory);
        return questionToUpdate;
    }

    @Transactional
    public Answer createAnswer(Answer answer) {
        return answerRepo.save(answer);
    }

    @Transactional
    public Answer updateAnswer(Integer qId, Integer aId, AnswerDto answerDto) {
        Question question = questionRepo.findById(qId)
                .orElseThrow(() -> new NotFoundException("Question does not exist"));

        Answer answerToUpdate = answerRepo.findById(aId)
                .orElseThrow(() -> new NotFoundException("Answer does not exist"));

        //Update only the content
        answerToUpdate.setContent(answerDto.getContent());
        return answerToUpdate;
    }

    @Transactional
    public Answer updateVote(Integer qId, Integer aId, String typeVote) {
        Question question = questionRepo.findById(qId)
                .orElseThrow(() -> new NotFoundException("Question does not exist"));

        Answer answerToUpdate = answerRepo.findById(aId)
                .orElseThrow(() -> new NotFoundException("Answer does not exist"));

        if (typeVote.equals("up")) {
            answerToUpdate.addPositiveVote();
        } else if (typeVote.equals("down")) {
            answerToUpdate.addNegativeVote();
        } else {
            throw new FunctionalException("Type vote is not supported");
        }
        return answerToUpdate;
    }
}