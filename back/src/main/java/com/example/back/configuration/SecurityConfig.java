package com.example.back.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

        auth
                .inMemoryAuthentication()
                .withUser("Alexandre")
                .password("{bcrypt}$2a$10$C5F8Xep67heLW.pxWGZpDOxY1SFuYAnnn4xsvz.XDjBDaWvskCMbK")
                .roles("USER", "ADMIN")

                .and()
                .withUser("Thibault")
                .password("{bcrypt}$2a$10$L2kmTwzRxvZWgdlOXmxKUOJIEQtN7sfB/WQ/H8L6erpqszjAk8eOu")
                .roles("USER");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
                .csrf().disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, "/api/test").hasAnyRole("USER", "ADMIN") //For controller test

                //CATEGORIES
                .antMatchers(HttpMethod.GET, "/api/categories").hasAnyRole("USER", "ADMIN") //To display categories
                .antMatchers(HttpMethod.GET, "/api/categories/*").hasAnyRole("USER", "ADMIN") //To display category by id or name
                .antMatchers(HttpMethod.POST, "/api/categories").hasRole("ADMIN") //To manage categories

                //STUDENTS
                .antMatchers(HttpMethod.GET, "/api/students").hasAnyRole("ADMIN, USER") //To display students
                .antMatchers(HttpMethod.GET, "/api/students/*").hasAnyRole("ADMIN", "USER") //To display student by id or username

                //QUESTIONS
                .antMatchers(HttpMethod.GET, "/api/questions").hasAnyRole("ADMIN", "USER")  //To display questions
                .antMatchers(HttpMethod.GET, "/api/questions/*").hasAnyRole("ADMIN", "USER")  //To display question by id
                .antMatchers(HttpMethod.GET, "/api/questions/?status=").hasAnyRole("ADMIN") //To display questions status
                .antMatchers(HttpMethod.GET, "/api/questions/categories/").hasAnyRole("ADMIN", "USER") //To display questions by category name
                .antMatchers(HttpMethod.POST, "/api/questions").hasAnyRole("ADMIN", "USER") //To create/update question
                .antMatchers(HttpMethod.GET, "/api/questions/user/*?status=DRAFT").hasAnyRole("ADMIN", "USER") //To display drafts by user

                //ANSWERS
                .antMatchers(HttpMethod.GET, "/api/answers/*").hasAnyRole("ADMIN", "USER")  //To display answer by id
                .antMatchers(HttpMethod.POST, "/api/questions/*/answers").hasAnyRole("ADMIN", "USER")  //To create/update answer
                .antMatchers(HttpMethod.POST, "/api/questions/*/answers/*/vote/*").hasAnyRole("ADMIN", "USER") //To vote on answers

                .anyRequest().authenticated()
                .and()
                .httpBasic()
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .cors();
    }
}
