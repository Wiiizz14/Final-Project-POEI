package com.example.back.models;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AnswerTest {

    //Data
    Category category = new Category("CategoryTest");
    Student student = new Student("Username", "Alex");
    Question question = new Question("Title", "Content", category, student);

    @Test
    void add_positive_vote() {
        //Init
        Answer answer = new Answer(question, student, "Response", 0);
        answer.addPositiveVote();

        //Test
        assertEquals(1, answer.getVote());
    }

    @Test
    void add_negative_vote() {
        //Init
        Answer answer = new Answer(question, student, "Response", 1);
        answer.addNegativeVote();

        //Test
        assertEquals(0, answer.getVote());
    }
}