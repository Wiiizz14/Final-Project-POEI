package com.example.back.it;

import com.example.back.EntitiesPersister;
import com.example.back.IntegrationTest;
import com.example.back.controllers.dtos.CategoryDto;
import com.example.back.exceptions.ErrorDto;
import com.example.back.models.Category;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URI;
import java.util.Objects;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@IntegrationTest
public class CategoryIT {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private EntitiesPersister entitiesPersister;

    @Test
    public void shouldReturnNotFoundUnknownCategory() {
        Integer unknownId = 100;
        ResponseEntity<ErrorDto> response =
                restTemplate
                        .withBasicAuth("Alexandre", "admin")
                        .getForEntity("/api/categories/{id}", ErrorDto.class, unknownId);

        //When get category
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(response.getBody()).isNotNull();
    }

    @Test
    public void shouldReturnExistingCategory() {
        Category category = new Category("CategoryTest");
        entitiesPersister.persist(category);

        //When get category
        ResponseEntity<CategoryDto> response =
                restTemplate
                        .withBasicAuth("Alexandre", "admin")
                        .getForEntity("/api/categories/{id}", CategoryDto.class, category.getId());

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull()
                .returns(category.getId(), CategoryDto::getId)
                .returns(category.getName(), CategoryDto::getName);
    }

    @Test
    public void shouldDeleteCategory() {
        Category category = new Category("CategoryTest");
        entitiesPersister.persist(category);

        //When delete category
        ResponseEntity<Void> deleteResponse =
                restTemplate
                        .withBasicAuth("Alexandre", "admin")
                        .exchange(URI.create("/api/categories/" + category.getId()), HttpMethod.DELETE, null, Void.class);

        assertThat(deleteResponse.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }

    @Test
    public void shouldCreateCategory() {
        CategoryDto inputDTO = new CategoryDto();
        inputDTO.setName("CategoryTest");

        //When create category
        ResponseEntity<CategoryDto> response =
                restTemplate
                        .withBasicAuth("Alexandre", "admin")
                        .postForEntity("/api/categories", inputDTO, CategoryDto.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(Objects.requireNonNull(response.getBody()).getId()).isNotNull();

        //When get category
        ResponseEntity<CategoryDto> getResponse =
                restTemplate
                        .withBasicAuth("Alexandre", "admin")
                        .getForEntity(URI.create("/api/categories/" + response.getBody().getId()), CategoryDto.class);

        assertThat(getResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(getResponse.getBody())
                .returns(inputDTO.getName(), CategoryDto::getName);
    }
}
