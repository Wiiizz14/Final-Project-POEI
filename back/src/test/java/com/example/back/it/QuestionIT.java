package com.example.back.it;

import com.example.back.EntitiesPersister;
import com.example.back.IntegrationTest;
import com.example.back.controllers.dtos.QuestionDto;
import com.example.back.exceptions.ErrorDto;
import com.example.back.models.Category;
import com.example.back.models.Question;
import com.example.back.models.Student;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Objects;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@IntegrationTest
public class QuestionIT {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private EntitiesPersister entitiesPersister;

    @Test
    public void shouldReturnNotFoundUnknownQuestion() {
        Integer unknownId = 100;
        ResponseEntity<ErrorDto> response =
                restTemplate
                        .withBasicAuth("Alexandre", "admin")
                        .getForEntity("/api/questions/{id}", ErrorDto.class, unknownId);

        //When get question
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(response.getBody()).isNotNull();
    }

    @Test
    public void shouldReturnExistingQuestion() {
        Student student = new Student("Username", "Email");
        Category category = new Category("CategoryTest");
        Question question = new Question("TitleTest", "ContentTest", category, student);
        entitiesPersister.persist(student, category, question);

        //When get question
        ResponseEntity<QuestionDto> response =
                restTemplate
                        .withBasicAuth("Alexandre", "admin")
                        .getForEntity("/api/questions/{id}", QuestionDto.class, question.getId());

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull()
                .returns(question.getId(), QuestionDto::getId)
                .returns(question.getTitle(), QuestionDto::getTitle)
                .returns(question.getContent(), QuestionDto::getContent)
                .returns(question.getStatus(), QuestionDto::getStatus)
                .returns(question.getCategory().getId(), QuestionDto::getCategoryId)
                .returns(question.getStudent().getId(), QuestionDto::getStudentId);
    }

    @Test
    public void shouldCreateQuestion() {
        Student student = new Student("Username", "Email");
        Category category = new Category("CategoryTest");
        entitiesPersister.persist(student, category);

        QuestionDto inputDto = new QuestionDto();
        inputDto.setTitle("TitleTest");
        inputDto.setContent("ContentTest");
        inputDto.setStatus("StatusTest");
        inputDto.setCategoryId(category.getId());
        inputDto.setStudentId(student.getId());

        //When create question
        ResponseEntity<QuestionDto> response =
                restTemplate
                        .withBasicAuth("Alexandre", "admin")
                        .postForEntity("/api/questions", inputDto, QuestionDto.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(Objects.requireNonNull(response.getBody()).getId()).isNotNull();
    }
}
