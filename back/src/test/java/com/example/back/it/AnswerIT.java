package com.example.back.it;

import com.example.back.EntitiesPersister;
import com.example.back.IntegrationTest;
import com.example.back.controllers.dtos.AnswerDto;
import com.example.back.exceptions.ErrorDto;
import com.example.back.models.Answer;
import com.example.back.models.Category;
import com.example.back.models.Question;
import com.example.back.models.Student;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Objects;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@IntegrationTest
public class AnswerIT {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private EntitiesPersister entitiesPersister;

    @Test
    public void shouldReturnNotFoundUnknownAnswer() {
        Student student = new Student("Username", "Email");
        Category category = new Category("CategoryTest");
        Question question = new Question("TitleTest", "ContentTest", category, student);
        entitiesPersister.persist(student, category, question);

        Integer unknownId = 100;
        ResponseEntity<ErrorDto> response =
                restTemplate
                        .withBasicAuth("Alexandre", "admin")
                        .getForEntity("/api/answers/{id}", ErrorDto.class, unknownId);

        //When get answer
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(response.getBody()).isNotNull();
    }

    @Test
    public void shouldReturnExistingAnswer() {
        Student student = new Student("Username", "Email");
        Category category = new Category("CategoryTest");
        Question question = new Question("TitleTest", "ContentTest", category, student);
        Answer answer = new Answer(question, student, "testContent", 123);
        entitiesPersister.persist(student, category, question, answer);

        //When get answer
        ResponseEntity<AnswerDto> response =
                restTemplate
                        .withBasicAuth("Alexandre", "admin")
                        .getForEntity("/api/answers/{id}", AnswerDto.class, answer.getId());

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull()
                .returns(answer.getId(), AnswerDto::getId)
                .returns(answer.getQuestion().getId(), AnswerDto::getQuestionId)
                .returns(answer.getStudent().getId(), AnswerDto::getStudentId)
                .returns(answer.getContent(), AnswerDto::getContent)
                .returns(answer.getVote(), AnswerDto::getVote);
    }

    @Test
    public void shouldCreateAnswer() {
        Student student = new Student("Username", "Email");
        Category category = new Category("CategoryTest");
        Question question = new Question("TitleTest", "ContentTest", category, student);
        entitiesPersister.persist(student, category, question);

        AnswerDto inputDto = new AnswerDto();
        inputDto.setQuestionId(question.getId());
        inputDto.setStudentId(student.getId());
        inputDto.setContent("ContentTest");
        inputDto.setVote(123);

        //When create answer
        ResponseEntity<AnswerDto> response =
                restTemplate
                        .withBasicAuth("Alexandre", "admin")
                        .postForEntity("/api/questions/{id}/answers", inputDto, AnswerDto.class, question.getId());

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(Objects.requireNonNull(response.getBody()).getId()).isNotNull();
    }
}
