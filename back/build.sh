#!/bin/bash

mvn clean verify
docker build -t registry.gitlab.com/zenika-poei-rennes-04/final-project-poei/back:latest .